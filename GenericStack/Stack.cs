﻿using System;
using System.Collections;

namespace GenericStack
{
    /// <summary>
    /// Represents extendable last-in-first-out (LIFO) collection of the specified type T.
    /// </summary>
    /// <typeparam name="T">Specifies the type of elements in the stack.</typeparam>
    public class Stack<T> : IEnumerable<T>
    {
        private const int DefaultCapacity = 10;
        private static T[] emptyArray = Array.Empty<T>();
        private T[] array;
        private int size;
        private int version;

        /// <summary>
        /// Initializes a new instance of the <see cref="Stack{T}" /> class that is empty and has the default initial capacity.
        /// </summary>
        public Stack()
        {
            this.array = emptyArray;
            this.size = 0;
            this.version = 0;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Stack{T}" /> class that is empty and has the specified initial capacity.
        /// </summary>
        /// <param name="capacity">The initial number of elements of stack.</param>
        public Stack(int capacity)
        {
            if (capacity < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(capacity));
            }

            this.array = new T[capacity];
            this.size = 0;
            this.version = 0;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Stack{T}" /> class that contains elements copied from the specified collection and has sufficient capacity to accommodate the number of elements copied.
        /// </summary>
        /// <param name="collection">The collection to copy elements from.</param>
        public Stack(IEnumerable<T>? collection)
        {
            if (collection is null)
            {
                throw new ArgumentNullException(nameof(collection));
            }

            ICollection<T>? c = collection as ICollection<T>;
            if (c != null)
            {
                int count = c.Count;
                this.array = new T[count];
                c.CopyTo(this.array, 0);
                this.size = count;
            }
            else
            {
                this.size = 0;
                this.array = new T[DefaultCapacity];

                using (IEnumerator<T> en = collection.GetEnumerator())
                {
                    while (en.MoveNext())
                    {
                        this.Push(en.Current);
                    }
                }
            }
        }

        /// <summary>
        /// Gets the number of elements contained in the stack.
        /// </summary>
        public int Count
        {
            get { return this.size; }
        }

        /// <summary>
        /// Removes and returns the object at the top of the stack.
        /// </summary>
        /// <returns>The object removed from the top of the stack.</returns>
        public T Pop()
        {
            if (this.size == 0)
            {
                throw new InvalidOperationException("Stack is empty");
            }

            this.version++;
            T item = this.array[--this.size];
            this.array[this.size] = default!;
            return item;
        }

        /// <summary>
        /// Returns the object at the top of the stack without removing it.
        /// </summary>
        /// <returns>The object at the top of the stack.</returns>
        public T Peek()
        {
            if (this.size == 0)
            {
                throw new InvalidOperationException("Stack is empty");
            }

            return this.array[this.size - 1];
        }

        /// <summary>
        /// Inserts an object at the top of the stack.
        /// </summary>
        /// <param name="item">The object to push onto the stack.
        /// The value can be null for reference types.</param>
        public void Push(T item)
        {
            if (this.size == this.array.Length)
            {
                T[] newArray = new T[(this.array.Length == 0) ? DefaultCapacity : 2 * this.array.Length];
                Array.Copy(this.array, 0, newArray, 0, this.size);
                this.array = newArray;
            }

            this.array[this.size++] = item;
            this.version++;
        }

        /// <summary>
        /// Copies the elements of stack to a new array.
        /// </summary>
        /// <returns>A new array containing copies of the elements of the stack.</returns>
        public T[] ToArray()
        {
            T[] objArray = new T[this.size];
            int i = 0;
            while (i < this.size)
            {
                objArray[i] = this.array[this.size - i - 1];
                i++;
            }

            return objArray;
        }

        /// <summary>
        /// Determines whether an element is in the stack.
        /// </summary>
        /// <param name="item">The object to locate in the stack. The value can be null for reference types.</param>
        /// <returns>Return true if item is found in the stack; otherwise, false.</returns>
        public bool Contains(T item)
        {
            int count = this.size;

            EqualityComparer<T> c = EqualityComparer<T>.Default;
            while (count-- > 0)
            {
                if (item == null)
                {
                    if (this.array[count] == null)
                    {
                        return true;
                    }
                }
                else if (this.array[count] != null && c.Equals(this.array[count], item))
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Removes all objects from the stack.
        /// </summary>
        public void Clear()
        {
            Array.Clear(this.array, 0, this.size);
            this.size = 0;
            this.version++;
        }

        /// <summary>
        /// Returns an enumerator for the stack.
        /// </summary>
        /// <returns>Return Enumerator object for the stack.</returns>
        public IEnumerator<T> GetEnumerator()
        {
            if (this.size == 0)
            {
                throw new InvalidOperationException("Стек пуст");
            }

            return new Enumerator(this);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        public struct Enumerator : IEnumerator<T>
        {
            private readonly Stack<T> stack;
            private readonly int version;
            private int index;
            private T? currentElement;

            internal Enumerator(Stack<T> stack)
            {
                this.stack = stack;
                this.version = this.stack.version;
                this.index = -2;
                this.currentElement = default;
            }

            public T Current
            {
                get
                {
                    if (this.index == -2)
                    {
                        throw new InvalidOperationException("Index is bad");
                    }

                    if (this.index == -1)
                    {
                        throw new InvalidOperationException("Index is bad");
                    }

                    return this.currentElement!;
                }
            }

            object System.Collections.IEnumerator.Current
            {
                get
                {
                    if (this.index == -2)
                    {
                        throw new InvalidOperationException("Index is bad");
                    }

                    if (this.index == -1)
                    {
                        throw new InvalidOperationException("Index is bad");
                    }

                    return this.currentElement!;
                }
            }

            public void Dispose()
            {
                this.index = -1;
            }

            public bool MoveNext()
            {
                bool retval;
                if (this.version != this.stack.version)
                {
                    throw new InvalidOperationException("Stack version is bad");
                }

                if (this.index == -2)
                {
                    this.index = this.stack.size - 1;
                    retval = this.index >= 0;
                    if (retval)
                    {
                        this.currentElement = this.stack.array[this.index];
                    }

                    return retval;
                }

                if (this.index == -1)
                {
                    return false;
                }

                retval = --this.index >= 0;
                if (retval)
                {
                    this.currentElement = this.stack.array[this.index];
                }
                else
                {
                    this.currentElement = default(T);
                }

                return retval;
            }

            void System.Collections.IEnumerator.Reset()
            {
                if (this.version != this.stack.version)
                {
                    throw new InvalidOperationException("Stack version is bad");
                }

                this.index = -2;
                this.currentElement = default(T);
            }
        }
    }
}
